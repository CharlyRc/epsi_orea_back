import mysql from "mysql2";
import { config } from 'dotenv';

config();

class BDDConfig {
    constructor() {
        this.pool = mysql.createPool({
            connectionLimit: 100,
            host: process.env.DATABASE_HOST,
            port: process.env.DATABASE_PORT,
            user: process.env.DATABASE_USER, 
            password: process.env.DATABASE_PASSWORD, 
            database: process.env.DATABASE_NAME, 
        });
    }

    async asyncQuery(sql, params = []) {
        return new Promise((resolve, reject) => {
            this.pool.query(sql, params, (error, elements) => {
                if (error) {
                    return reject(error);
                }
                return resolve(elements);
            });
        });
    }
}

const BDD  = new BDDConfig();
export default BDD ;