import express from "express"
import router from "./routes/routes.js"
import cors from "cors"
import bodyParser from "body-parser"
import swaggerConfig from "./swaggerConfig.js";
// permettant d'analyser JSON

const app = express();
const PORT = 3001;

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended:true }));
app.use(express.static("public"));
app.use("/", router);

swaggerConfig(app);

const server = app.listen(PORT, () => {
    console.log(`Le serveur est démarré sur le port ${PORT}`);
});

export default server;