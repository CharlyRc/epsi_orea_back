/**
 * @swagger
 * /products:
 *   post:
 *     summary: Créer un nouveau produit avec des images
 *     tags: 
 *       - Products
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - name
 *               - description
 *               - price
 *               - files
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Nom du produit"
 *               description:
 *                 type: string
 *                 example: "Description du produit"
 *               price:
 *                 type: number
 *                 example: 19.99
 *               files:
 *                 type: string
 *                 example: "chemin/vers/image.jpg"
 *     responses:
 *       201:
 *         description: Succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   properties:
 *                     result:
 *                       type: object
 *                       properties:
 *                         insertId:
 *                           type: integer
 *                           example: 1
 *                 dataPicture:
 *                   type: object
 *                   properties:
 *                     result:
 *                       type: object
 *                       properties:
 *                         insertId:
 *                           type: integer
 *                           example: 1
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Product from "../model/Product.js";
import Pictures from "../model/Pictures.js";

export default async (req, res) => {
    
    const {name, description, price, files} = req.body;
    
    try {
        const pictures = new Pictures(BDD);
        const product = new Product(BDD);
        // insérer les infos dans la base de données
        const data = await product.create ({name, description, price});
        // on récupère l'identifiant du produit
        const product_id = data.result.insertId;
        // insérer les images dans la base de données avec les infos
        const dataPicture = await pictures.create({url:files,caption:name,product_id:product_id});
        res.status(201).json({data, dataPicture});
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};