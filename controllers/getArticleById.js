/**
 * @swagger
 * /articles/{id}:
 *   get:
 *     summary: Obtenir un article par ID
 *     tags: 
 *       - Articles
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               id:
 *                 type: integer
 *                 description: ID de l'article à obtenir
 *                 example: 1
 *     responses:
 *       200:
 *         description: Article récupéré avec succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   example: { id: 1, title: "Titre de l'article", description: "Description de l'article" }
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Article from "../model/Article.js";

export default async (req, res) => {
    const {id} = req.body;
    try {
        const article = new Article(BDD);
        const data = await article.getById({id});
        res.status(200).json({data});
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};