/**
 * @swagger
 * /admin:
 *   put:
 *     summary: Mettre à jour un utilisateur
 *     tags:
 *       - Admin
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               last_name:
 *                 type: string
 *                 example: "Doe"
 *               first_name:
 *                 type: string
 *                 example: "John"
 *               email:
 *                 type: string
 *                 example: "john.doe@example.com"
 *               id:
 *                 type: integer
 *                 example: 1
 *     responses:
 *       200:
 *         description: Utilisateur mis à jour avec succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   properties:
 *                     message:
 *                       type: string
 *                       example: "Utilisateur mis à jour avec succès"
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Users from "../model/Users.js";

export default async (req, res) => {
    const {last_name, first_name, email, id} = req.body;
    try {
        const user = new Users(BDD);
        const data = await user.update ({last_name, first_name, email, id});
        res.status(200).json({data});
        console.log(data);
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};