/**
 * @swagger
 * /picture-articles/{id}:
 *   get:
 *     summary: Obtenir les images d'un article par ID
 *     tags: 
 *       - Articles-pictures
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: integer
 *         description: ID de l'article pour lequel obtenir les images
 *     responses:
 *       200:
 *         description: Images de l'article récupérées avec succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                         example: 1
 *                       url:
 *                         type: string
 *                         example: "/uploads/article_image.jpg"
 *                       caption:
 *                         type: string
 *                         example: "Image de l'article"
 *                       article_id:
 *                         type: integer
 *                         example: 1
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Articles_pictures from "../model/Articles_pictures.js";

export default async (req, res) => {
    const {id} = req.params;
    try {
        const article = new Articles_pictures(BDD);
        const data = await article.getByArticleId({article_id:id});
        res.status(200).json({data});
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};