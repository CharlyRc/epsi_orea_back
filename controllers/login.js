import BDD from "../config/database.js";
import Users from "../model/Users.js";
import {generateToken} from "../config/token.js";

export default async (req, res) => {
    const {email, password} = req.body;
    try {
        const admin = new Users(BDD);
        const data = await admin.login({email, password, generateToken});
        res.json({data});
        console.log(data);
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};
