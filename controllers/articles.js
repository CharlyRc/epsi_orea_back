/**
 * @swagger
 * /articles:
 *   get:
 *     summary: Récupérer tous les articles
 *     tags: 
 *       - Articles
 *     responses:
 *       200:
 *         description: Liste de tous les articles
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                         example: 1
 *                       title:
 *                         type: string
 *                         example: "Titre de l'article"
 *                       description:
 *                         type: string
 *                         example: "Description de l'article"
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Article from "../model/Article.js";

export default async (req, res) => {
    try {
        const article = new Article(BDD);
        const data = await article.getAll();
        res.json({data});
    }catch(err) {
        console.log(err);
        res.status(200).sendStatus(500);
    }
};
