/**
 * @swagger
 * /picture-products/{id}:
 *   get:
 *     summary: Obtenir les images d'un produit par ID
 *     tags: 
 *       - Products-pictures
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: integer
 *         description: ID du produit pour lequel obtenir les images
 *     responses:
 *       200:
 *         description: Images du produit récupérées avec succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                         example: 1
 *                       url:
 *                         type: string
 *                         example: "/uploads/product_image.jpg"
 *                       caption:
 *                         type: string
 *                         example: "Image du produit"
 *                       product_id:
 *                         type: integer
 *                         example: 1
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Pictures from "../model/Pictures.js";

export default async (req, res) => {
    const {id} = req.params;
    try {
        const picture = new Pictures(BDD);
        const data = await picture.getByProductId({product_id:id});
        res.status(200).json({data});
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};