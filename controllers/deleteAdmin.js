/**
 * @swagger
 * /admin/{id}:
 *   delete:
 *     summary: Supprimer un utilisateur par ID
 *     tags: 
 *       - Admin
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: integer
 *         description: ID de l'utilisateur à supprimer
 *     responses:
 *       204:
 *         description: Utilisateur supprimé avec succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   example: { affectedRows: 1 }
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Users from "../model/Users.js";

export default async (req, res) => {
    const {id} = req.params;
    try {
        const user = new Users(BDD);
        const data = await user.deleted({id});
        res.status(204).json({data});
        console.log(data);
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};