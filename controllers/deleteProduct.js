/**
 * @swagger
 * /products/{id}:
 *   delete:
 *     summary: Supprimer un produit par ID
 *     tags: 
 *       - Products
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: integer
 *         description: ID du produit à supprimer
 *     responses:
 *       204:
 *         description: Produit supprimé avec succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   example: { affectedRows: 1 }
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Product from "../model/Product.js";
import deleteFile from "../config/deleteFile.js";
import Pictures from "../model/Pictures.js";

export default async (req, res) => {
    const {id} = req.params;
    try {
        const product = new Product(BDD);
        const picture = new Pictures(BDD);
        // récupérer les données de l'image associée au produit
        const dataProduct = await picture.getByProductId({product_id:id});
        // Supprimer le fichier image associé au produit
        await deleteFile(dataProduct.result[0].url);
        // supprimer le produit
        const data = await product.deleted({id});
        res.status(204).json({data});
        console.log(data);
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};