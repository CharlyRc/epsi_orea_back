/**
 * @swagger
 * /admin:
 *   post:
 *     summary: Créer un nouvel administrateur
 *     tags: 
 *       - Admin
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - last_name
 *               - first_name
 *               - email
 *               - password
 *             properties:
 *               last_name:
 *                 type: string
 *                 example: Doe
 *               first_name:
 *                 type: string
 *                 example: John
 *               email:
 *                 type: string
 *                 example: john.doe@example.com
 *               password:
 *                 type: string
 *                 example: password123
 *     responses:
 *       201:
 *         description: Succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   example: { last_name: "Doe", first_name: "John", email: "john.doe@example.com", password: "password123" }
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Users from "../model/Users.js";

export default async (req, res) => {
    // On extrait les valeurs des paramètres envoyés dans le corps de la requête HTTP.
    const {last_name, first_name, email, password} = req.body;
    try {
        //gérer les utilisateurs de cette base de données.
        const admin = new Users(BDD);
        //on enregistre les informations de l'utilisateur dans la base de données
        const data = await admin.register ({last_name, first_name, email, password});
        res.status(201).json({ data });
        console.log(data);
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};