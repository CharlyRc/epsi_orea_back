/**
 * @swagger
 * /products/{id}:
 *   get:
 *     summary: Obtenir un produit par ID
 *     tags: 
 *       - Products
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: integer
 *         description: ID du produit à obtenir
 *     responses:
 *       200:
 *         description: Produit récupéré avec succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   example: { id: 1, name: "Nom du produit", description: "Description du produit", price: 19.99 }
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Product from "../model/Product.js";

export default async (req, res) => {
    const {id} = req.body;
    try {
        const product = new Product(BDD);
        const data = await product.getById({id});
        res.status(200).json({data});
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};