/**
 * @swagger
 * /contacts/{id}:
 *   delete:
 *     summary: Supprimer un contact par ID
 *     tags: 
 *       - Contacts
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: integer
 *         description: ID du contact à supprimer
 *     responses:
 *       204:
 *         description: Contact supprimé avec succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   example: { affectedRows: 1 }
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Contact from "../model/Contact.js";

export default async (req, res) => {
    const {id} = req.params;
    try {
        const contact = new Contact(BDD);
        const data = await contact.deleted({id});
        res.status(204).json({data});
        console.log(data);
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};