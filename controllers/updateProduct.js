/**
 * @swagger
 * /products:
 *   put:
 *     summary: Mettre à jour un produit
 *     tags:
 *       - Products
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Nouveau nom de produit"
 *               description:
 *                 type: string
 *                 example: "Nouvelle description de produit"
 *               price:
 *                 type: number
 *                 example: 20.99
 *               id:
 *                 type: integer
 *                 example: 1
 *     responses:
 *       200:
 *         description: Produit mis à jour avec succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   properties:
 *                     message:
 *                       type: string
 *                       example: "Produit mis à jour avec succès"
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Product from "../model/Product.js";

export default async (req, res) => {
    const {name, description, price, id,} = req.body;
    try {
        const product = new Product(BDD);
        const data = await product.update ({name, description, price, id});
        res.status(200).json({data});
        console.log(data);
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};