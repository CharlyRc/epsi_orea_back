/**
 * @swagger
 * /contacts:
 *   post:
 *     summary: Créer un nouveau contact
 *     tags: 
 *       - Contacts
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - last_name
 *               - first_name
 *               - address
 *               - city
 *               - code_postal
 *               - telephone
 *               - mail
 *               - message
 *             properties:
 *               last_name:
 *                 type: string
 *                 example: "Doe"
 *               first_name:
 *                 type: string
 *                 example: "John"
 *               address:
 *                 type: string
 *                 example: "123 Rue Exemple"
 *               city:
 *                 type: string
 *                 example: "Exempleville"
 *               code_postal:
 *                 type: string
 *                 example: "12345"
 *               telephone:
 *                 type: string
 *                 example: "0123456789"
 *               mail:
 *                 type: string
 *                 example: "john.doe@example.com"
 *               message:
 *                 type: string
 *                 example: "Ceci est un message."
 *     responses:
 *       201:
 *         description: Succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   example: { id: 1, last_name: "Doe", first_name: "John", address: "123 Rue Exemple", city: "Exempleville", code_postal: "12345", telephone: "0123456789", mail: "john.doe@example.com", message: "Ceci est un message." }
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Contact from "../model/Contact.js";

export default async (req, res) => {
    // on récupère les données du formulaire
    const {last_name, first_name, address, city, code_postal, telephone, mail, message} = req.body;
    
    try {
        const contact = new Contact(BDD);
        // insérer les infos dans la base de données
        const data = await contact.create ({last_name, first_name, address, city, code_postal, telephone, mail, message});
        res.status(201).json({data});
        console.log(data);
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};