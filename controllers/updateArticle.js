/**
 * @swagger
 * /articles:
 *   put:
 *     summary: Mettre à jour un article
 *     tags:
 *       - Articles
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *                 example: "Nouveau titre de l'article"
 *               description:
 *                 type: string
 *                 example: "Nouvelle description de l'article"
 *               id:
 *                 type: integer
 *                 example: 1
 *     responses:
 *       200:
 *         description: Article mis à jour avec succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   properties:
 *                     message:
 *                       type: string
 *                       example: "Article mis à jour avec succès"
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Article from "../model/Article.js";

export default async (req, res) => {
    const {title, description, id,} = req.body;
    try {
        const article = new Article(BDD);
        const data = await article.update ({title, description, id});
        res.status(200).json({data});
        console.log(data);
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};