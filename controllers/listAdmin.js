/**
 * @swagger
 * /admin:
 *   get:
 *     summary: Obtenir tous les utilisateurs
 *     tags: 
 *       - Admin
 *     responses:
 *       200:
 *         description: Liste des utilisateurs récupérée avec succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                         example: 1
 *                       username:
 *                         type: string
 *                         example: "utilisateur1"
 *                       email:
 *                         type: string
 *                         example: "utilisateur1@example.com"
 *                       created_at:
 *                         type: string
 *                         format: date-time
 *                         example: "2024-06-16T12:00:00Z"
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Users from "../model/Users.js";

export default async (req, res) => {
    try {
        const user = new Users(BDD);
        const data = await user.getAll();
        res.status(200).json({data});
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};