/**
 * @swagger
 * /contacts:
 *   get:
 *     summary: Obtenir tous les contacts
 *     tags: 
 *       - Contacts
 *     responses:
 *       200:
 *         description: Liste des contacts récupérée avec succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                         example: 1
 *                       last_name:
 *                         type: string
 *                         example: "Doe"
 *                       first_name:
 *                         type: string
 *                         example: "John"
 *                       address:
 *                         type: string
 *                         example: "123 Rue des Contacts"
 *                       city:
 *                         type: string
 *                         example: "Paris"
 *                       code_postal:
 *                         type: string
 *                         example: "75000"
 *                       telephone:
 *                         type: string
 *                         example: "+33 6 12 34 56 78"
 *                       mail:
 *                         type: string
 *                         example: "john.doe@example.com"
 *                       message:
 *                         type: string
 *                         example: "Bonjour, j'ai une question..."
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Contact from "../model/Contact.js";

export default async (req, res) => {
    try {
        const contact = new Contact(BDD);
        const data = await contact.getAll();
        res.status(200).json({data});
        console.log(data);
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};