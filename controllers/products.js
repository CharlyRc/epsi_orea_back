/**
 * @swagger
 * /products:
 *   get:
 *     summary: Obtenir tous les produits
 *     tags: 
 *       - Products
 *     responses:
 *       200:
 *         description: Liste des produits récupérée avec succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                         example: 1
 *                       name:
 *                         type: string
 *                         example: "Produit A"
 *                       description:
 *                         type: string
 *                         example: "Description du Produit A"
 *                       price:
 *                         type: number
 *                         example: 19.99
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Product from "../model/Product.js";

export default async (req, res) => {
    try {
        const product = new Product(BDD);
        const data = await product.getAll();
        res.status(200).json({data});
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};