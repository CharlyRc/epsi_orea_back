/**
 * @swagger
 * /articles:
 *   post:
 *     summary: Créer un nouvel article avec des images
 *     tags: 
 *       - Articles
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - title
 *               - description
 *               - files
 *             properties:
 *               title:
 *                 type: string
 *                 example: "Titre de l'article"
 *               description:
 *                 type: string
 *                 example: "Description de l'article"
 *               files:
 *                 type: string
 *                 example: "chemin/vers/image.jpg"
 *     responses:
 *       201:
 *         description: Succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   properties:
 *                     result:
 *                       type: object
 *                       properties:
 *                         insertId:
 *                           type: integer
 *                           example: 1
 *                 dataPicture:
 *                   type: object
 *                   properties:
 *                     result:
 *                       type: object
 *                       properties:
 *                         insertId:
 *                           type: integer
 *                           example: 1
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Article from "../model/Article.js";
import Articles_pictures from "../model/Articles_pictures.js";

export default async (req, res) => {
    // on récupère les données du formulaire
    const {title, description, files} = req.body;
    
    try {
        const pictureArticle = new Articles_pictures(BDD);
        const article = new Article(myBDD);
        // insérer les infos dans la base de données
        const data = await article.create ({title, description});
         // on récupère l'identifiant de l'article
        const article_id = data.result.insertId;
        // insérer les images dans la base de données avec les infos
        const dataPicture = await pictureArticle.create({url:files,caption:title,article_id:article_id});
        // renvoyer les données à l'utilisateur
        res.status(201).json({data, dataPicture});
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};