import { verifyToken } from "../config/token.js";
import parseurl from 'parseurl';

const ADMIN = 'admin';
const PUBLIC = 'public';

// Listes des routes protégées par méthode HTTP
const protectedRoutes = {
    GET: [
        "/admin",
        "/admin/:id",
        "/contacts",
    ],
    POST: [
        "/admin",
        "/products",
        "/articles",
        "/contacts",
    ],
    PUT: [
        "/admin",
        "/products",
        "/articles",
        "/picture-products/:id",
        "/picture-articles/:id",
    ],
    DELETE: [
        "/admin/:id",
        "/articles/:id",
        "/products/:id",
        "/contacts/:id"
    ],
};
const isProtectedRoute = (pathname, method) => {
    const routes = protectedRoutes[method] || [];
    return routes.some(route => new RegExp(route.replace(/:\w+/g, '\\w+')).test(pathname));
};

const protectedPath = (pathname, method) => {
    const protectedAdmin = isProtectedRoute(pathname, method);
    return protectedAdmin ? ADMIN : PUBLIC;
};

const accesAutorized = (pathname, method, userData) => {
    const typePath = protectedPath(pathname, method);

    const adminAcess = userData && userData.admin ? typePath === ADMIN : false;
    const publicAcess = typePath === PUBLIC;

    console.log((publicAcess || adminAcess));

    return (publicAcess || adminAcess) ? true : false;
};

export default async (req, res, next) => {
    if (process.env.NODE_ENV === 'test') {
        return next(); // Ignore l'authentification en mode test
    }

    const pathname = parseurl(req).pathname;
    const method = req.method;

    console.log(pathname);

    const headersAuth = req.headers['authorization'];
    const token = headersAuth ? headersAuth.split(' ')[1] : null;

    try {
        const userData = await verifyToken(token);
        const acces = accesAutorized(pathname, method, userData);
        const response = { response: false, msg: 'accès refusé' };

        return acces ? next() : res.json(response);
    } catch (err) {
        console.log(err);
        res.sendStatus(401);
    }
};