/**
 * @swagger
 * /articles/{id}:
 *   delete:
 *     summary: Supprimer un article par ID
 *     tags: 
 *       - Articles
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: integer
 *         description: ID de l'article à supprimer
 *     responses:
 *       204:
 *         description: Article supprimé avec succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   example: { affectedRows: 1 }
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Article from "../model/Article.js";
import deleteFile from "../config/deleteFile.js";
import Articles_pictures from "../model/Articles_pictures.js";

export default async (req, res) => {
    const {id} = req.params;
    try {
        const article = new Article(BDD);
        const pictureArticle = new Articles_pictures(BDD);
        // récupérer les données de l'image associée à l'article
        const dataArticle = await pictureArticle.getByArticleId({article_id:id});
        // Supprimer le fichier image associé à l'article
        await deleteFile(dataArticle.result[0].url);
        // Supprimer l'article
        const data = await article.deleted({id});
        res.status(204).json({data});
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};