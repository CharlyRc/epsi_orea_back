/**
 * @swagger
 * /admin/{id}:
 *   get:
 *     summary: Obtenir un utilisateur par ID
 *     tags: 
 *       - Admin
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               id:
 *                 type: integer
 *                 description: ID de l'utilisateur à obtenir
 *                 example: 1
 *     responses:
 *       200:
 *         description: Utilisateur récupéré avec succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   example: { id: 1, last_name: "Doe", first_name: "John", email: "john.doe@example.com" }
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Users from "../model/Users.js";

export default async (req, res) => {
    const {id} = req.body;
    try {
        const user = new Users(BDD);
        const data = await user.getById({id});
        res.status(200).json({data});
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};