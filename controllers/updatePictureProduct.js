/**
 * @swagger
 * /picture-products/:id:
 *   put:
 *     summary: Mettre à jour une image de produit
 *     tags:
 *       - Products-pictures
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               files:
 *                 type: string
 *                 example: "http://example.com/image.jpg"
 *               caption:
 *                 type: string
 *                 example: "Nouvelle légende de l'image"
 *               id:
 *                 type: integer
 *                 example: 1
 *               product_id:
 *                 type: integer
 *                 example: 1
 *     responses:
 *       200:
 *         description: Image de produit mise à jour avec succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   properties:
 *                     message:
 *                       type: string
 *                       example: "Image de produit mise à jour avec succès"
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Pictures from "../model/Pictures.js";

export default async (req, res) => {
    const {files, caption, id, product_id} = req.body;
    console.log({files, caption, id, product_id});
    try {
        const picture = new Pictures(BDD);
        const data = await picture.update({url:files, caption, id, product_id});
        res.status(200).json({data});
        console.log(data);
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};