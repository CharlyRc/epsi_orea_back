/**
 * @swagger
 * /picture-articles/:id:
 *   put:
 *     summary: Mettre à jour une image d'article
 *     tags:
 *       - Articles-pictures
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               files:
 *                 type: string
 *                 example: "http://example.com/image.jpg"
 *               caption:
 *                 type: string
 *                 example: "Nouvelle légende de l'image"
 *               id:
 *                 type: integer
 *                 example: 1
 *               article_id:
 *                 type: integer
 *                 example: 1
 *     responses:
 *       200:
 *         description: Image d'article mise à jour avec succès
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   properties:
 *                     message:
 *                       type: string
 *                       example: "Image d'article mise à jour avec succès"
 *       500:
 *         description: Erreur du serveur
 */
import BDD from "../config/database.js";
import Articles_pictures from "../model/Articles_pictures.js";

export default async (req, res) => {
    const {files, caption, id, article_id} = req.body;
    console.log({files, caption, id, article_id});
    try {
        const article = new Articles_pictures(BDD);
        const data = await article.update({url:files, caption, id, article_id});
        res.status(200).json({data});
        console.log(data);
    }catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
};