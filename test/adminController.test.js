import request from 'supertest';
import app from '../server.js';
jest.mock('../config/database.js');
jest.mock('../model/Users');

import BDD from '../config/database.js';

BDD.asyncQuery = jest.fn();

import Users from '../model/Users';
Users.mockImplementation((bdd) => {
  return {
    register: jest.fn(async ({ last_name, first_name, email, password }) => {
      return { last_name, first_name, email, password };
    }),
  };
});

jest.mock('../config/token.js', () => {
  return {
    verifyToken: jest.fn(async (token) => {
      if (token === 'valid-token') {
        return { admin: true };
      }
      throw new Error('Invalid token');
    }),
  };
});

describe('POST /admin', () => {
  afterAll(async () => {
    await new Promise(resolve => setTimeout(() => resolve(), 500));
    app.close();
  });

  it('should create a new admin', async () => {
    const newAdmin = {
      last_name: 'Doe',
      first_name: 'John',
      email: 'john.doe@example.com',
      password: 'password123'
    };

    const response = await request(app)
      .post('/admin')
      .set('Authorization', 'Bearer valid-token')
      .send(newAdmin)
      .expect(201);

    expect(response.body).toHaveProperty('data');
    expect(response.body.data).toMatchObject(newAdmin);
  });
});
