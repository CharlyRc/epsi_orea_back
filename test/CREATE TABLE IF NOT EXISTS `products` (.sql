CREATE TABLE IF NOT EXISTS `products` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    `description` longtext NOT NULL,
    `price` longtext NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 141 DEFAULT CHARSET = latin1 COLLATE = latin1_swedish_ci;
-- Insertion de données dans 'products'
INSERT INTO `products` (`id`, `name`, `description`, `price`)
VALUES (
        128,
        'Tarte tatin pomme',
        'Pâte sablée breton sans gluten, pommes caramélisées, ganache montée vanille, caramel au beurre salé',
        'Œufs, lactose'
    ),
    (
        129,
        'Tarte citron noisette',
        'Pâte sucrée noisette, crème d\'amande noisette, crémeux citron jaune, meringue italienne',
        'Fruits à coques, gluten, lactose, oeufs'
    ),
    (
        130,
        'Choco’noisette',
        'Croustillant praliné sans gluten, biscuit cacao sans gluten, insert praliné et crémeux chocolat noir, mousse chocolat lait et noir',
        'Oeufs, lactose, fruits à coques (noisettes)'
    ),
    (
        131,
        'Paris-Brest',
        'Pâte à choux croustillante, crème mousseline praliné noisette, praliné pur noisette, noisettes concassées',
        'Gluten, fruits à coques, lactose, oeufs'
    ),
    (
        132,
        'Cookies',
        'Déclinaison de saveurs (classique, caramel, cacahuètes, maxi noix ...)',
        'Fruits à coques, gluten, oeufs, lactose'
    ),
    (
        133,
        'Brownies',
        'Pâte sucrée amande cacao, brownie chocolat noir noix, caramel beurre salé',
        'Gluten, oeufs, lactose, fruits à coques'
    ),
    (
        134,
        'Eclairs classiques',
        'Déclinaison de différents parfums à base de crémeux (caramel, citron meringué, framboise, chocolat)',
        'Gluten, lactose, œufs'
    ),
    (
        135,
        'Eclair chocolat fleur de sel',
        'Pâte sablée amande cacao, pâte à choux, craquelin cacao, crémeux chocolat noir, fleur de sel',
        'Gluten, lactose, fruits à coque, œufs'
    ),
    (
        136,
        'Tropiques',
        'Pâte sablée amande, biscuit moelleux amande, compotée et brunoise de mangues, confit banane passion, mousse mangue, glaçage passion',
        'Fruits à coques (amandes), lait, gluten, oeufs'
    ),
    (
        137,
        'Tarte aux fraises',
        'Pâte sucrée vanille, crème d\'amande, crémeux citron fraise, fraises',
        'Lactose, oeufs'
    ),
    (139, 'test', 'test', 'test');
-- Création de la table 'pictures'
CREATE TABLE IF NOT EXISTS `pictures` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `product_id` int(11) NOT NULL,
    `url` varchar(511) NOT NULL,
    `caption` varchar(255) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `product_id` (`product_id`),
    CONSTRAINT `pictures_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 68 DEFAULT CHARSET = latin1 COLLATE = latin1_swedish_ci;
-- Insertion de données dans 'pictures'
INSERT INTO `pictures` (`id`, `product_id`, `url`, `caption`)
VALUES (55, 128, '324166855a7d562462b41d600.jpg', 'test'),
    (
        56,
        129,
        '324166855a7d562462b41d601.jpeg',
        'test'
    ),
    (
        57,
        130,
        '324166855a7d562462b41d602.jpeg',
        'Choco’noisette'
    ),
    (
        58,
        131,
        '324166855a7d562462b41d603.jpeg',
        'Paris-Brest'
    ),
    (
        59,
        132,
        '324166855a7d562462b41d604.jpg',
        'Cookies'
    ),
    (
        60,
        133,
        '324166855a7d562462b41d605.jpg',
        'Brownies'
    ),
    (
        61,
        134,
        '324166855a7d562462b41d606.jpg',
        'Eclairs classiques'
    ),
    (
        62,
        135,
        '324166855a7d562462b41d607.jpg',
        'Eclair chocolat fleur de sel'
    ),
    (
        63,
        136,
        '324166855a7d562462b41d608.jpeg',
        'Tropiques'
    ),
    (
        64,
        137,
        '324166855a7d562462b41d609.jpg',
        'Tarte aux fraises'
    ),
    (66, 139, 'e50b1b9d8d8639c2433e05400.png', 'test');