import express from "express";
import middleware from "../controllers/middleware.js";
import middlewareUpload from "../controllers/middlewareUpload.js";
import login from "../controllers/login.js";
import addAdmin from "../controllers/addAdmin.js";
import listAdmin from "../controllers/listAdmin.js";
import getAdminById from "../controllers/getAdminById.js";
import deleteAdmin from "../controllers/deleteAdmin.js";
import updateAdmin from "../controllers/updateAdmin.js";
import products from "../controllers/products.js";
import addProduct from "../controllers/addProduct.js";
import deleteProduct from "../controllers/deleteProduct.js";
import updateProduct from "../controllers/updateProduct.js";
import getProductById from "../controllers/getProductById.js";
import getPictureProductById from "../controllers/getPictureProductById.js";
import updatePictureProduct from "../controllers/updatePictureProduct.js";
import articles from "../controllers/articles.js";
import addArticle from "../controllers/addArticle.js";
import deleteArticle from "../controllers/deleteArticle.js";
import updateArticle from "../controllers/updateArticle.js";
import getArticleById from "../controllers/getArticleById.js";
import getPictureArticleById from "../controllers/getPictureArticleById.js";
import updatePictureArticle from "../controllers/updatePictureArticle.js";
import contactForm from "../controllers/contactForm.js";
import listContact from "../controllers/listContact.js";
import deleteContact from "../controllers/deleteContact.js";
import checkToken from '../controllers/checkToken.js';

const router = express.Router();

const routesGET = [
    {route:"/products", controller:products},
    {route:"/articles", controller:articles},
    {route:"/contacts", controller:listContact},
    {route:"/admin", controller:listAdmin},
    {route:"/admin/:id", controller:getAdminById},
    {route:"/products/:id", controller:getProductById},
    {route:"/articles/:id", controller:getArticleById},
    {route:"/picture-products/:id", controller:getPictureProductById},
    {route:"/picture-articles/:id", controller:getPictureArticleById},
    {route:"/relogged", controller:checkToken},
];
const routesPOST = [
    {route:"/admin", controller:addAdmin},
    {route:"/login", controller:login},
    {route:"/contacts", controller:contactForm},
];

const routesPUT = [
    {route:"/admin", controller:updateAdmin},
    {route:"/products", controller:updateProduct},
    {route:"/articles", controller:updateArticle},
];

const routesDELETE = [
    {route:"/admin/:id", controller:deleteAdmin},
    {route:"/articles/:id", controller:deleteArticle},
    {route:"/products/:id", controller:deleteProduct},
    {route:"/contacts/:id", controller:deleteContact},
];

routesGET.map((item) =>{
        router.get(item.route, middleware, item.controller);

});

routesPOST.map((item) =>{
        router.post(item.route, middleware, item.controller);
});

routesPUT.map((item) =>{
    router.put(item.route, middleware, item.controller);
});

routesDELETE.map((item) =>{
    router.delete(item.route, middleware, item.controller);
});

router.post("/products", middleware, middlewareUpload, addProduct);
router.post("/articles", middleware, middlewareUpload, addArticle);
router.put("/picture-products/:id", middleware, middlewareUpload, updatePictureProduct);
router.put("/picture-articles/:id", middleware, middlewareUpload, updatePictureArticle);

export default router;