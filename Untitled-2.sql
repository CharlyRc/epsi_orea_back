-- --------------------------------------------------------
-- Hôte:                         127.0.0.1
-- Version du serveur:           11.1.2-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win64
-- HeidiSQL Version:             12.3.0.6589
-- --------------------------------------------------------
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */
;
/*!40101 SET NAMES utf8 */
;
/*!50503 SET NAMES utf8mb4 */
;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */
;
/*!40103 SET TIME_ZONE='+00:00' */
;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */
;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */
;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */
;
-- Listage de la structure de la base pour orea
CREATE DATABASE IF NOT EXISTS `orea`
/*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci */
;
USE `orea`;
-- Listage de la structure de table orea. admin
CREATE TABLE IF NOT EXISTS `admin` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `last_name` varchar(255) NOT NULL,
    `first_name` varchar(255) NOT NULL,
    `email` varchar(255) NOT NULL,
    `password` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 27 DEFAULT CHARSET = latin1 COLLATE = latin1_swedish_ci;
-- Listage des données de la table orea.admin : ~2 rows (environ)
INSERT INTO `admin` (
        `id`,
        `last_name`,
        `first_name`,
        `email`,
        `password`
    )
VALUES (
        3,
        'dupont-test',
        'david',
        'blabla@admin.fr',
        '$2b$10$NZTYpi2ii5EyuN0.SFiY5OLWn7nSZOk9udU2X8o7B5GIMQLwFancW'
    ),
    (
        26,
        'dupont',
        'david',
        'test@test.fr',
        '$2b$10$bxegi9vHrHskOu9YfUFB4.Y3WeitjdJTzECL5olCm.Q9GiwLfBdYi'
    );
-- Listage de la structure de table orea. articles
CREATE TABLE IF NOT EXISTS `articles` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `title` varchar(255) NOT NULL,
    `description` longtext NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 28 DEFAULT CHARSET = latin1 COLLATE = latin1_swedish_ci;
-- Listage des données de la table orea.articles : ~6 rows (environ)
INSERT INTO `articles` (`id`, `title`, `description`)
VALUES (
        21,
        'La remorque est arrivée !',
        'Après un long périple de 600 km, j\'ai enfin pu admirer ma remorque aménagée réalisée sur mesure par l\'entreprise Beau Comme Un Camion. J\'espère commencer les marchés à la rentrée. Au plaisir de se rencontrer, Ophélie.\n\n'
    ),
    (
        22,
        'Partenariat Ô Piment Doux x Oréa Pâtisserie',
        'Je suis ravie de vous annoncer ma collaboration avec le traiteur Ô Piment Doux pour la partie évènementielle. Mylaine est spécialisée dans la cuisine réunionnaise et est basée à Chateaubriant (44). Une association pleine de saveurs du plat jusqu\'au dessert !\r\n\r\n'
    ),
    (
        23,
        'Article Presse Océan',
        'Merci à Presse Océan pour ce bel article paru le 10/11/2023 !\r\n\r\n'
    ),
    (
        24,
        'Fêtes de fin d\'année 2023',
        'Les fêtes de fin d\'année approchent !\r\nVoici mon tout premier flyer pour cette période festive.\r\nLes bûches et gourmandises sont disponibles en quantités limitées.\r\nVous pouvez dès à présent me contacter pour passer commande.'
    ),
    (
        25,
        'Maxi BN !',
        'Confection d’un BN pour les 10 ans de @badnantes pour 50 personnes\r\nPourquoi le BN ? Pour les initiales Bad’ Nantes et bien sûr pour les biscuits qui coulent dans nos veines de Nantais '
    ),
    (
        26,
        'Number cake du jour',
        'Pour les 50 ans d\'un client, voici une nouveauté avec ca gâteau vanille spéculoos, chocolat caramel\r\n\r\nMerci pour votre confiance !'
    );
-- Listage de la structure de table orea. articles_pictures
CREATE TABLE IF NOT EXISTS `articles_pictures` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `article_id` int(11) NOT NULL,
    `url` varchar(511) NOT NULL,
    `caption` varchar(127) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `product_id` (`article_id`),
    CONSTRAINT `articles_pictures_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 26 DEFAULT CHARSET = latin1 COLLATE = latin1_swedish_ci;
-- Listage des données de la table orea.articles_pictures : ~6 rows (environ)
INSERT INTO `articles_pictures` (`id`, `article_id`, `url`, `caption`)
VALUES (19, 21, '324166855a7d562462b41d60a.jpg', 'test'),
    (
        20,
        22,
        '324166855a7d562462b41d60b.jpg',
        'Partenariat Ô Piment Doux x Oréa Pâtisserie'
    ),
    (
        21,
        23,
        '324166855a7d562462b41d60c.jpg',
        'Article Presse Océan'
    ),
    (
        22,
        24,
        '127f5900f4952fd742104aa01.png',
        'Fêtes de fin d\'année 2023'
    ),
    (
        23,
        25,
        '324166855a7d562462b41d60f.png',
        'Maxi BN !'
    ),
    (
        24,
        26,
        '324166855a7d562462b41d610.png',
        'Number cake du jour'
    );
-- Listage de la structure de table orea. contact
CREATE TABLE IF NOT EXISTS `contact` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `last_name` varchar(255) NOT NULL,
    `first_name` varchar(255) NOT NULL,
    `address` varchar(255) NOT NULL,
    `city` varchar(255) NOT NULL,
    `code_postal` varchar(255) NOT NULL,
    `telephone` varchar(255) NOT NULL,
    `mail` varchar(255) NOT NULL,
    `message` longtext NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 24 DEFAULT CHARSET = latin1 COLLATE = latin1_swedish_ci;
-- Listage des données de la table orea.contact : ~4 rows (environ)
INSERT INTO `contact` (
        `id`,
        `last_name`,
        `first_name`,
        `address`,
        `city`,
        `code_postal`,
        `telephone`,
        `mail`,
        `message`
    )
VALUES (
        20,
        'test',
        'test',
        '1 Impasse Denis Papin apt 108',
        'hrth',
        '44444',
        '0202020202',
        'fzfer.fzef@frefe.fer',
        'jpuoigt'
    ),
    (
        21,
        'test',
        'test',
        '1 Impasse Denis Papin apt 108',
        'hrth',
        '00000',
        '0202020202',
        'fzfer.fzef@frefe.fer',
        'oguyfyifyguihijiojiojo'
    ),
    (
        22,
        'Ricoul',
        'Charly',
        '1 Impasse Denis Papin apt 108',
        'hrth',
        '00000',
        '0202020202',
        'fzfer.fzef@frefe.fer',
        'lmhjfgfuyguiom'
    ),
    (
        23,
        'Ricoul',
        'test',
        'test',
        'hrth',
        '44444',
        '0202020202',
        'fzfer.fzef@frefe.fer',
        'jhohuhi'
    );
-- Listage de la structure de table orea. pictures
CREATE TABLE IF NOT EXISTS `pictures` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `product_id` int(11) NOT NULL,
    `url` varchar(511) NOT NULL,
    `caption` varchar(255) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `product_id` (`product_id`),
    CONSTRAINT `pictures_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 68 DEFAULT CHARSET = latin1 COLLATE = latin1_swedish_ci;
-- Listage des données de la table orea.pictures : ~11 rows (environ)
INSERT INTO `pictures` (`id`, `product_id`, `url`, `caption`)
VALUES (55, 128, '324166855a7d562462b41d600.jpg', 'test'),
    (
        56,
        129,
        '324166855a7d562462b41d601.jpeg',
        'test'
    ),
    (
        57,
        130,
        '324166855a7d562462b41d602.jpeg',
        'Choco’noisette'
    ),
    (
        58,
        131,
        '324166855a7d562462b41d603.jpeg',
        'Paris-Brest'
    ),
    (
        59,
        132,
        '324166855a7d562462b41d604.jpg',
        'Cookies'
    ),
    (
        60,
        133,
        '324166855a7d562462b41d605.jpg',
        'Brownies'
    ),
    (
        61,
        134,
        '324166855a7d562462b41d606.jpg',
        'Eclairs classiques'
    ),
    (
        62,
        135,
        '324166855a7d562462b41d607.jpg',
        'Eclair chocolat fleur de sel'
    ),
    (
        63,
        136,
        '324166855a7d562462b41d608.jpeg',
        'Tropiques'
    ),
    (
        64,
        137,
        '324166855a7d562462b41d609.jpg',
        'Tarte aux fraises'
    ),
    (66, 139, 'e50b1b9d8d8639c2433e05400.png', 'test');
-- Listage de la structure de table orea. products
CREATE TABLE IF NOT EXISTS `products` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    `description` longtext NOT NULL,
    `price` longtext NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 141 DEFAULT CHARSET = latin1 COLLATE = latin1_swedish_ci;
-- Listage des données de la table orea.products : ~11 rows (environ)
INSERT INTO `products` (`id`, `name`, `description`, `price`)
VALUES (
        128,
        'Tarte tatin pomme',
        'Pâte sablée breton sans gluten, pommes caramélisées, ganache montée vanille, caramel au beurre salé',
        'Œufs, lactose'
    ),
    (
        129,
        'Tarte citron noisette',
        'Pâte sucrée noisette, crème d\'amande noisette, crémeux citron jaune, meringue italienne',
        'Fruits à coques, gluten, lactose, oeufs'
    ),
    (
        130,
        'Choco’noisette',
        'Croustillant praliné sans gluten, biscuit cacao sans gluten, insert praliné et crémeux chocolat noir, mousse chocolat lait et noir',
        'Oeufs, lactose, fruits à coques (noisettes)'
    ),
    (
        131,
        'Paris-Brest',
        'Pâte à choux croustillante, crème mousseline praliné noisette, praliné pur noisette, noisettes concassées',
        'Gluten, fruits à coques, lactose, oeufs'
    ),
    (
        132,
        'Cookies',
        'Déclinaison de saveurs (classique, caramel, cacahuètes, maxi noix ...)',
        'Fruits à coques, gluten, oeufs, lactose'
    ),
    (
        133,
        'Brownies',
        'Pâte sucrée amande cacao, brownie chocolat noir noix, caramel beurre salé',
        'Gluten, oeufs, lactose, fruits à coques'
    ),
    (
        134,
        'Eclairs classiques',
        'Déclinaison de différents parfums à base de crémeux (caramel, citron meringué, framboise, chocolat)',
        'Gluten, lactose, œufs'
    ),
    (
        135,
        'Eclair chocolat fleur de sel',
        'Pâte sablée amande cacao, pâte à choux, craquelin cacao, crémeux chocolat noir, fleur de sel',
        'Gluten, lactose, fruits à coque, œufs'
    ),
    (
        136,
        'Tropiques',
        'Pâte sablée amande, biscuit moelleux amande, compotée et brunoise de mangues, confit banane passion, mousse mangue, glaçage passion',
        'Fruits à coques (amandes), lait, gluten, oeufs'
    ),
    (
        137,
        'Tarte aux fraises',
        'Pâte sucrée vanille, crème d\'amande, crémeux citron fraise, fraises',
        'Lactose, oeufs'
    ),
    (139, 'test', 'test', 'test');
/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */
;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */
;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */
;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */
;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */
;